import pandas as pd
import numpy as np
import scipy.stats as stats
from scipy.stats import chi2_contingency
from scipy.stats import chisquare


from sklearn.feature_selection import SelectKBest, SelectPercentile
from sklearn.feature_selection import chi2

global importantFeatures
importantFeatures = ["patientID", "timestamp"]


class ChiSquare:
    def __init__(self, dataframe):
        self.df = dataframe
        self.p = None
        self.chi2 = None
        self.dof = None

        self.dfObserved = None
        self.dfExpected = None

    def _print_chisquare_result(self, colX, alpha):
        global importantFeatures
        result = ""
        if self.p < alpha:
            importantFeatures.append(colX)
            result = "{0} is IMPORTANT for Prediction".format(colX)
        else:
            result = "{0} is NOT an important predictor. (Discard {0} from model)".format(
                colX)

        print(result)

    def TestIndependence(self, colX, colY, alpha=0.05):
        X = self.df[colX].astype(str)
        Y = self.df[colY].astype(str)

        self.dfObserved = pd.crosstab(Y, X)
        chi2, p, dof, expected = stats.chi2_contingency(self.dfObserved.values)
        self.p = p
        self.chi2 = chi2
        self.dof = dof

        self.dfExpected = pd.DataFrame(
            expected, columns=self.dfObserved.columns, index=self.dfObserved.index)

        self._print_chisquare_result(colX, alpha)


df = pd.pandas.read_csv("out.csv")

cT = ChiSquare(df)

testColumns = ["attributeCode", "attributeValue", "avgAllBlood",
               "avgUnspecified", "avgPre", "avgPost", "avgUnsp", "avg0to6", "avg6to12",
               "avg12to18", "avg18to24", "avg6to18", "avgPreBreakfast", "avgPreLunch",
               "avgPreSupper", "avgPreSnack", "avgPreAll", "avgPostBreakfast", "avgPostLunch",
               "avgPostSupper", "avgPostSnack", "avgPostAll", "avg0to6Blood", "avg6to12Blood",
               "avg12to18Blood", "avg18to24Blood", "avg0to6Food", "avg6to12Food", "avg12to18Food",
               "avg18to24Food", "avg0to6Activity", "avg6to12Activity", "avg12to18Activity",
               "avg18to24Activity", "avgLast4Hours", "avgLast6Hours", "avgLast1andHalfHours",
               "avgLast24Hours", "avgLast2Hours", "avgLast1Hours", "avgRegularLast6Hours",
               "avgNPHLast14Hours", "avgUltralenteLast24Hours", "maxRegularLast6Hours",
               "maxNPHLast14Hours", "maxUltarelenteLast24Houts", "totalActivityLast2Hours",
               "totalActivityLast4Hours", "totalActivityLast6Hours", "totalActivityLast12Hours",
               "totalActivityLast24Hours", "avgLast12Hours", "avgActivityLast2Hours",
               "avgActivityLast4Hours", "avgActivityLast6Hours", "avgActivityLast12Hours", "avgActivityLast24Hours"
               ]

for var in testColumns:
    cT.TestIndependence(colX=var, colY="attributeValue")

print(importantFeatures)
new_csv = df[importantFeatures]
new_csv.to_csv("out_chisquare.csv", index=False)

percentlist = [10, 25, 50, 75, 90]

for p in percentlist:
    n = ((len(importantFeatures) * p) // 100)
    array = importantFeatures[:n]
    new_csv = df[array]
    filename = "out_chisquare_" + str(p) + ".csv"
    new_csv.to_csv(filename, index=False)
    print(array)
