from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVR
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score

import pandas as pd
import numpy as np


featureToPredict = "attributeValue"
df = pd.pandas.read_csv("out_chisquare.csv")
if(np.any(np.isnan(df))):
    df.fillna(method='ffill', inplace=True)


def NaiveBayes():
    gnb = GaussianNB()

    y = df[featureToPredict]
    X = df.drop([featureToPredict], axis=1)

    X_warmup_train, X_use, y_warmup_train, y_use = train_test_split(
        X, y, train_size=0.2, shuffle=False)
    X_train, X_test, y_train, y_test = train_test_split(
        X_use, y_use, test_size=0.25, shuffle=False)

    gnb.fit(X_train, y_train.ravel())
    prediction = gnb.predict(X_test)

    mse = mean_squared_error(y_test, prediction, squared=True)
    print("MSE:", mse)

    rmse = mean_squared_error(y_test, prediction, squared=False)
    print("RMSE:", rmse)

    accuracy = accuracy_score(y_test, prediction)
    print("Accuracy:", accuracy)


def svm():
    regressor = SVR()
    y = df[featureToPredict]
    X = df.drop([featureToPredict], axis=1)

    X_warmup_train, X_use, y_warmup_train, y_use = train_test_split(
        X, y, train_size=0.2, shuffle=False)
    X_train, X_test, y_train, y_test = train_test_split(
        X_use, y_use, test_size=0.25, shuffle=False)

    regressor.fit(X_train, y_train.ravel())
    prediction = regressor.predict(X_test)

    mse = mean_squared_error(y_test, prediction, squared=True)
    print("MSE:", mse)

    rmse = mean_squared_error(y_test, prediction, squared=False)
    print("RMSE:", rmse)


def main():
    NaiveBayes()
    svm()


main()
