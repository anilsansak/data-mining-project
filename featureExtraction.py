import json
import csv
from datetime import datetime

global dataFilePath, outputCSVFilePath
dataFilePath = "data.json"
outputCSVFilePath = "out_2.csv"


def readFile(filepath):
    print("Reading file:", filepath)
    with open(filepath) as json_file:
        data = json.load(json_file)
        return data["attributes"], data["data"]


def stripData(attributes, data):
    print("Stripping all non-blood sugar measurements...")
    strippedData = []
    for patient in data:
        for attribute in attributes:
            if int(patient["attributeCode"]) == int(attribute["attrCode"]):
                if attribute["isBloodSugar"] == 1:
                    strippedData.append(patient)
    return strippedData


def writeToCSV(corpus):
    global outputCSVFilePath
    with open(outputCSVFilePath, 'w', encoding='utf8', newline='') as output_file:
        fc = csv.DictWriter(output_file,
                            fieldnames=corpus[0].keys(),

                            )
        fc.writeheader()
        fc.writerows(corpus)


def writeToJSON(corpus, attributes):
    global dataFilePath
    with open(dataFilePath, "w") as f:
        d = []
        d.append({"attributes": attributes})
        d.append({"data": corpus})
        f.write(json.dumps(d, sort_keys=False, indent=4,
                           separators=(',', ': ')))  # for pretty


def cleanCorpus(corpus):
    print("Cleaning corpus...")
    cleanedCorpus = []
    for item in corpus:
        try:
            int(item["attributeValue"])
            cleanedCorpus.append(item)
        except ValueError:
            print("attribute value is not an integer")
    return cleanedCorpus


def extractGeneralFeatures(corpus, attributes):
    # General Features:
    #   * Average of all blood sugar levels up to that date.
    #   * Average of unspecified blood sugar levels up to that date.
    #   * Average of all blood sugar levels in the same category(pre- post- or unspecified, whicever group the measurement belongs to) up to that date.
    #   * Average of all blood sugar levels up to that date within the same time zone.
    #   - Average of all blood sugar levels in the same category up to that date within the same time zone.
    #   - Average of all blood sugar levels up to that date both in same measurement group (pre- post- unspecified) and in the same time zone.
    print("Extracting General Features...")

    corpus = stripData(attributes, corpus)

    unspecifiedBloodSugarCodes = []
    preBloodSugarCodes = []
    postBloodSugarCodes = []

    for attr in attributes:
        if attr["isBloodSugar"] == 1:
            if ("Unspecified" in attr["description"]):
                unspecifiedBloodSugarCodes.append(attr["attrCode"])
            if ("Pre" in attr["description"]):
                preBloodSugarCodes.append(attr["attrCode"])
            if ("Post" in attr["description"]):
                postBloodSugarCodes.append(attr["attrCode"])

    for patient in corpus:
        allTotal = 0
        unspecifiedTotal = 0
        countUnspecified = 0

        preTotal = 0
        countPre = 0
        postTotal = 0
        countPost = 0
        countTotal = 0

        total0to6 = 0
        count0to6 = 0

        total6to12 = 0
        count6to12 = 0

        total12to18 = 0
        count12to18 = 0

        total18to24 = 0
        count18to24 = 0

        for p in corpus:
            allTotal += int(p["attributeValue"])

            hour = datetime.fromtimestamp(p["timestamp"]).hour
            if 0 <= hour < 6 or hour == 24:
                total0to6 += int(p["attributeValue"])
                count0to6 += 1
            if 6 <= hour < 12:
                total6to12 += int(p["attributeValue"])
                count6to12 += 1
            if 12 <= hour < 18:
                total12to18 += int(p["attributeValue"])
                count12to18 += 1
            if 18 <= hour < 24:
                total18to24 += int(p["attributeValue"])
                count18to24 += 1

            for code in unspecifiedBloodSugarCodes:
                if int(p["attributeCode"]) == code:
                    unspecifiedTotal += int(p["attributeValue"])
                    countUnspecified += 1
            for code in preBloodSugarCodes:
                if int(p["attributeCode"]) == code:
                    preTotal += int(p["attributeValue"])
                    countPre += 1
            for code in postBloodSugarCodes:
                if int(p["attributeCode"]) == code:
                    postTotal += int(p["attributeValue"])
                    countPost += 1
            if p is patient:
                break

        allAverage = allTotal / (corpus.index(patient) + 1)
        unspecifiedAverage = 0
        preAverage = 0
        postAverage = 0
        avg0to6 = 0
        avg6to12 = 0
        avg12to18 = 0
        avg18to24 = 0
        avg6to18 = 0
        if countUnspecified > 0:
            unspecifiedAverage = unspecifiedTotal / countUnspecified
        if countPre > 0:
            preAverage = preTotal / countPre
        if countPost > 0:
            postAverage = postTotal / countPost

        if count0to6 > 0:
            avg0to6 = total0to6 / count0to6
        if count6to12 > 0:
            avg6to12 = total6to12 / count6to12
        if count12to18 > 0:
            avg12to18 = total12to18 / count12to18
        if count18to24 > 0:
            avg18to24 = total18to24 / count18to24
        if count6to12 > 0 and count12to18 > 0:
            avg6to18 = (total6to12 + total12to18) / (count6to12 + count12to18)

        patient["avgAllBlood"] = format(allAverage, ".2f")
        patient["avgUnspecified"] = format(unspecifiedAverage, ".2f")
        patient["avgPre"] = format(preAverage, ".2f")
        patient["avgPost"] = format(postAverage, ".2f")
        patient["avgUnsp"] = format(unspecifiedAverage, ".2f")

        patient["avg0to6"] = format(avg0to6, ".2f")
        patient["avg6to12"] = format(avg6to12, ".2f")
        patient["avg12to18"] = format(avg12to18, ".2f")
        patient["avg18to24"] = format(avg18to24, ".2f")
        patient["avg6to18"] = format(avg6to18, ".2f")

    print("General Feature Extraction has been completed.")
    return corpus


def extractBloodSugarFeatures(corpus, attributes):
    # Blood Sugar Level Features:
    #   * Average of all pre-breakfast blood sugar levels up to that date
    #   * Average of all pre-lunch blood sugar levels up to that date.
    #   * Average of all pre-supper Blood sugar levels up to that date.
    #   * Average of all pre-snack Blood sugar levels up to that date.
    #   * Average of all pre-all (only remove unspecified measurements) Blood sugar levels up to that date.
    #   * Average of all post-breakfast Blood sugar levels up to that date.
    #   * Average of all post-lunch Blood sugar levels up to that date.
    #   * Average of all post-supper Blood sugar levels up to that date.
    #   * Average of all post-snack Blood sugar levels up to that date.
    #   * Average of all post-all (only remove unspecified measurements) Blood sugar levels up to that date.
    print("Extracting Blood Sugar Level Features...")
    # corpus = stripData(attributes, corpus)

    preBreakfastSugarLevelCodes = []
    preLunchSugarLevelCodes = []
    preSupperSugarLevelCodes = []
    preSnackSugarLevelCodes = []
    preAllSugarLevelCodes = []
    postBreakfastSugarLevelCodes = []
    postLunchSugarLevelCodes = []
    postSupperSugarLevelCodes = []
    postSnackSugarLevelCodes = []
    postAllSugarLevelCodes = []

    for attr in attributes:
        if attr["isBloodSugar"] == 1:
            if ("Pre-breakfast" in attr["description"]):
                preBreakfastSugarLevelCodes.append(attr["attrCode"])
            if ("Pre-lunch" in attr["description"]):
                preLunchSugarLevelCodes.append(attr["attrCode"])
            if ("Pre-supper" in attr["description"]):
                preSupperSugarLevelCodes.append(attr["attrCode"])
            if ("Pre-snack" in attr["description"]):
                preSnackSugarLevelCodes.append(attr["attrCode"])
            if ("Post-breakfast" in attr["description"]):
                postBreakfastSugarLevelCodes.append(attr["attrCode"])
            if ("Post-lunch" in attr["description"]):
                postLunchSugarLevelCodes.append(attr["attrCode"])
            if ("Post-supper" in attr["description"]):
                postSupperSugarLevelCodes.append(attr["attrCode"])
            if ("Post-snack" in attr["description"]):
                postSnackSugarLevelCodes.append(attr["attrCode"])

    for patient in corpus:
        preBreakfastTotal = 0
        countPreBreakfast = 0

        preLunchTotal = 0
        countPreLunch = 0

        preSupperTotal = 0
        countPreSupper = 0

        preSnackTotal = 0
        countPreSnack = 0

        postBreakfastTotal = 0
        countPostBreakfast = 0

        postLunchTotal = 0
        countPostLunch = 0

        postSupperTotal = 0
        countPostSupper = 0

        postSnackTotal = 0
        countPostSnack = 0
        for p in corpus:
            pCode = int(p["attributeCode"])
            pValue = int(p["attributeValue"])
            for code in preBreakfastSugarLevelCodes:
                if pCode == code:
                    preBreakfastTotal += pValue
                    countPreBreakfast += 1
            for code in preLunchSugarLevelCodes:
                if pCode == code:
                    preLunchTotal += pValue
                    countPreLunch += 1
            for code in preSupperSugarLevelCodes:
                if pCode == code:
                    preSupperTotal += pValue
                    countPreSupper += 1
            for code in preSnackSugarLevelCodes:
                if pCode == code:
                    preSnackTotal += pValue
                    countPreSnack += 1
            for code in postBreakfastSugarLevelCodes:
                if pCode == code:
                    postBreakfastTotal += pValue
                    countPostBreakfast += 1
            for code in postLunchSugarLevelCodes:
                if pCode == code:
                    postLunchTotal += pValue
                    countPostLunch += 1
            for code in postSupperSugarLevelCodes:
                if pCode == code:
                    postSupperTotal += pValue
                    countPostSupper += 1
            for code in postSnackSugarLevelCodes:
                if pCode == code:
                    postSnackTotal += pValue
                    countPostSnack += 1
            if p is patient:
                break

        avgPreBreakfast = 0
        avgPreLunch = 0
        avgPreSupper = 0
        avgPreSnack = 0
        avgPostBreakfast = 0
        avgPostLunch = 0
        avgPostSupper = 0
        avgPostSnack = 0

        avgPreAll = 0
        avgPostAll = 0

        if countPreBreakfast > 0:
            avgPreBreakfast = preBreakfastTotal / countPreBreakfast
        if countPreLunch > 0:
            avgPreLunch = preLunchTotal / countPreLunch
        if countPreSupper > 0:
            avgPreSupper = preSupperTotal / countPreSupper
        if countPreSnack > 0:
            avgPreSnack = preSnackTotal / countPreSnack
        if countPostBreakfast > 0:
            avgPostBreakfast = postBreakfastTotal / countPostBreakfast
        if countPostLunch > 0:
            avgPostLunch = postLunchTotal / countPostLunch
        if countPostSupper > 0:
            avgPostSupper = postSupperTotal / countPostSupper
        if countPostSnack > 0:
            avgPostSnack = postSnackTotal / countPostSnack

        if countPreBreakfast > 0 and countPreLunch > 0 and countPreSupper > 0 and countPreSnack > 0:
            avgPreAll = (preBreakfastTotal + preLunchTotal + preSupperTotal + preSnackTotal) / \
                (countPreBreakfast + countPreLunch + countPreSupper + countPreSnack)
        if countPostBreakfast > 0 and countPostLunch > 0 and countPostSupper > 0 and countPostSnack > 0:
            avgPostAll = (postBreakfastTotal + postLunchTotal + postSupperTotal + postSnackTotal) / \
                (countPostBreakfast + countPostLunch +
                 countPostSupper + countPostSnack)

        patient["avgPreBreakfast"] = format(avgPreBreakfast, ".2f")
        patient["avgPreLunch"] = format(avgPreLunch, ".2f")
        patient["avgPreSupper"] = format(avgPreSupper, ".2f")
        patient["avgPreSnack"] = format(avgPreSnack, ".2f")
        patient["avgPreAll"] = format(avgPreAll, ".2f")
        patient["avgPostBreakfast"] = format(avgPostBreakfast, ".2f")
        patient["avgPostLunch"] = format(avgPostLunch, ".2f")
        patient["avgPostSupper"] = format(avgPostSupper, ".2f")
        patient["avgPostSnack"] = format(avgPostSnack, ".2f")
        patient["avgPostAll"] = format(avgPostAll, ".2f")

    print("Blood Sugar Level Features extraction has been completed.")
    return corpus


def extractTimeRelatedFeatures(corpus, attributes):
    # Time Related Features
    #   * Average of all 00-06 Blood sugar levels up to that date.
    #   * Average of all 06-12 Blood sugar levels up to that date.
    #   * Average of all 12-18 Blood sugar levels up to that date.
    #   * Average of all 18-24 Blood sugar levels up to that date.
    #   * Average of all 00-06 Food Intake levels up to that date.
    #   * Average of all 06-12 Food Intake levels up to that date.
    #   * Average of all 12-18 Food Intake levels up to that date.
    #   * Average of all 18-24 Food Intake levels up to that date.
    #   * Average of all 00-06 Activity levels up to that date.
    #   * Average of all 06-12 Activity levels up to that date.
    #   * Average of all 12-18 Activity levels up to that date.
    #   * Average of all 18-24 Activity levels up to that date.
    #   - Average of all Blood Sugar levels in the same time zone.
    #   - Average of all Food Intake levels in the same time zone.
    #   - Average of all Activity levels in the same time zone.
    print("Extracting Time Related Features...")

    bloodSugarLevelCodes = []
    foodIntakeLevelCodes = []
    activityLevelCodes = []

    for attr in attributes:
        if attr["isBloodSugar"] == 1:
            bloodSugarLevelCodes.append(attr["attrCode"])
        if attr["isMeal"] == 1:
            foodIntakeLevelCodes.append(attr["attrCode"])
        if attr["isExercise"] == 1:
            activityLevelCodes.append(attr["attrCode"])

    for patient in corpus:
        total0to6Blood = 0
        count0to6Blood = 0

        total6to12Blood = 0
        count6to12Blood = 0

        total12to18Blood = 0
        count12to18Blood = 0

        total18to24Blood = 0
        count18to24Blood = 0

        total0to6Food = 0
        count0to6Food = 0

        total6to12Food = 0
        count6to12Food = 0

        total12to18Food = 0
        count12to18Food = 0

        total18to24Food = 0
        count18to24Food = 0

        total0to6Activity = 0
        count0to6Activity = 0

        total6to12Activity = 0
        count6to12Activity = 0

        total12to18Activity = 0
        count12to18Activity = 0

        total18to24Activity = 0
        count18to24Activity = 0

        for p in corpus:
            hour = datetime.fromtimestamp(float(patient["timestamp"])).hour
            pCode = int(p["attributeCode"])
            pValue = int(p["attributeValue"])

            if 0 <= hour < 6 or hour == 24:
                for code in bloodSugarLevelCodes:
                    if code == pCode:
                        total0to6Blood += pValue
                        count0to6Blood += 1
                for code in foodIntakeLevelCodes:
                    if code == pCode:
                        total0to6Food += pValue
                        count0to6Food += 1
                for code in activityLevelCodes:
                    if code == pCode:
                        total0to6Activity += pValue
                        count0to6Activity += 1
            if 6 <= hour < 12:
                for code in bloodSugarLevelCodes:
                    if code == pCode:
                        total6to12Blood += pValue
                        count6to12Blood += 1
                for code in foodIntakeLevelCodes:
                    if code == pCode:
                        total6to12Food += pValue
                        count6to12Food += 1
                for code in activityLevelCodes:
                    if code == pCode:
                        total6to12Activity += pValue
                        count6to12Activity += 1
            if 12 <= hour < 18:
                for code in bloodSugarLevelCodes:
                    if code == pCode:
                        total12to18Blood += pValue
                        count12to18Blood += 1
                for code in foodIntakeLevelCodes:
                    if code == pCode:
                        total12to18Food += pValue
                        count12to18Food += 1
                for code in activityLevelCodes:
                    if code == pCode:
                        total12to18Activity += pValue
                        count12to18Activity += 1
            if 18 <= hour < 24:
                for code in bloodSugarLevelCodes:
                    if code == pCode:
                        total18to24Blood += pValue
                        count18to24Blood += 1
                for code in foodIntakeLevelCodes:
                    if code == pCode:
                        total18to24Food += pValue
                        count18to24Food += 1
                for code in activityLevelCodes:
                    if code == pCode:
                        total18to24Activity += pValue
                        count18to24Activity += 1
            if p is patient:
                break
        avg0to6Blood = 0
        avg6to12Blood = 0
        avg12to18Blood = 0
        avg18to24Blood = 0

        avg0to6Food = 0
        avg6to12Food = 0
        avg12to18Food = 0
        avg18to24Food = 0

        avg0to6Activity = 0
        avg6to12Activity = 0
        avg12to18Activity = 0
        avg18to24Activity = 0

        if count0to6Blood > 0:
            avg0to6Blood = total0to6Blood / count0to6Blood
        if count6to12Blood > 0:
            avg6to12Blood = total6to12Blood / count6to12Blood
        if count12to18Blood > 0:
            avg12to18Blood = total12to18Blood / count12to18Blood
        if count18to24Blood > 0:
            avg18to24Blood = total18to24Blood / count18to24Blood

        if count0to6Food > 0:
            avg0to6Food = total0to6Food / count0to6Food
        if count6to12Food > 0:
            avg6to12Food = total6to12Food / count6to12Food
        if count12to18Food > 0:
            avg12to18Food = total12to18Food / count12to18Food
        if count18to24Food > 0:
            avg18to24Food = total18to24Food / count18to24Food

        if count0to6Activity > 0:
            avg0to6Activity = total0to6Activity / count0to6Activity
        if count6to12Activity > 0:
            avg6to12Activity = total6to12Activity / count6to12Activity
        if count12to18Activity > 0:
            avg12to18Activity = total12to18Activity / count12to18Activity
        if count18to24Activity > 0:
            avg18to24Activity = total18to24Activity / count18to24Activity

        patient["avg0to6Blood"] = format(avg0to6Blood, ".2f")
        patient["avg6to12Blood"] = format(avg6to12Blood, ".2f")
        patient["avg12to18Blood"] = format(avg12to18Blood, ".2f")
        patient["avg18to24Blood"] = format(avg18to24Blood, ".2f")

        patient["avg0to6Food"] = format(avg0to6Food, ".2f")
        patient["avg6to12Food"] = format(avg6to12Food, ".2f")
        patient["avg12to18Food"] = format(avg12to18Food, ".2f")
        patient["avg18to24Food"] = format(avg18to24Food, ".2f")

        patient["avg0to6Activity"] = format(avg0to6Activity, ".2f")
        patient["avg6to12Activity"] = format(avg6to12Activity, ".2f")
        patient["avg12to18Activity"] = format(avg12to18Activity, ".2f")
        patient["avg18to24Activity"] = format(avg18to24Activity, ".2f")

    print("Time related features extraction has been completed.")
    return corpus


def extractFoodIntakeFeatures(corpus, attribues):
    # Food Intake Features
    #   * Average of Food Intake in the last 4 hours.
    #   * Average of Food Intake in the last 6 hours.
    #   * Average of Food Intake in the last 1.5 hours.
    #   * Average of Food Intake in the last 24 hours.
    #   * Average of Food Intake in the last 2 hours.
    #   * Average of Food Intake in the last 1 hours.
    #   - Time Elapsed since last meal (in hours, rounded down)
    #   - Time Elapsed since last meal (in minutes, rounded down)

    print("Extracting Food Intake Features...")
    foodIntakeCodes = []

    for attr in attribues:
        if attr["isMeal"] == 1:
            foodIntakeCodes.append(attr["attrCode"])

    for patient in corpus:
        totalLast4Hours = 0
        countLast4Hours = 0

        totalLast6Hours = 0
        countLast6Hours = 0

        totalLast1andHalfHours = 0
        countLast1andHalfHours = 0

        totalLast24Hours = 0
        countLast24Hours = 0

        totalLast2Hours = 0
        countLast2Hours = 0

        totalLast1Hours = 0
        countLast1Hours = 0

        patientTimestamp = float(patient["timestamp"])

        for p in corpus:
            pCode = int(p["attributeCode"])
            pValue = int(p["attributeValue"])
            pTimestamp = float(p["timestamp"])

            if pTimestamp <= patientTimestamp - (3600 * 4):
                for code in foodIntakeCodes:
                    if code == pCode:
                        totalLast4Hours += pValue
                        countLast4Hours += 1
            if pTimestamp <= patientTimestamp - (3600 * 6):
                for code in foodIntakeCodes:
                    if code == pCode:
                        totalLast6Hours += pValue
                        countLast6Hours += 1
            if pTimestamp <= patientTimestamp - (3600 * 1.5):
                for code in foodIntakeCodes:
                    if code == pCode:
                        totalLast1andHalfHours += pValue
                        countLast1andHalfHours += 1
            if pTimestamp <= patientTimestamp - (3600 * 24):
                for code in foodIntakeCodes:
                    if code == pCode:
                        totalLast24Hours += pValue
                        countLast24Hours += 1
            if pTimestamp <= patientTimestamp - (3600 * 2):
                for code in foodIntakeCodes:
                    if code == pCode:
                        totalLast2Hours += pValue
                        countLast2Hours += 1
            if pTimestamp <= patientTimestamp - (3600 * 1):
                for code in foodIntakeCodes:
                    if code == pCode:
                        totalLast1Hours += pValue
                        countLast1Hours += 1
        avgLast4Hours = 0
        avgLast6Hours = 0
        avgLast1andHalfHours = 0
        avgLast24Hours = 0
        avgLast2Hours = 0
        avgLast1Hours = 0

        if countLast4Hours > 0:
            avgLast4Hours = totalLast4Hours / countLast4Hours
        if countLast6Hours > 0:
            avgLast6Hours = totalLast6Hours / countLast6Hours
        if countLast1andHalfHours > 0:
            avgLast1andHalfHours = totalLast1andHalfHours / countLast1andHalfHours
        if countLast24Hours > 0:
            avgLast24Hours = totalLast24Hours / countLast24Hours
        if countLast2Hours > 0:
            avgLast2Hours = totalLast2Hours / countLast2Hours
        if countLast1Hours > 0:
            avgLast1Hours = totalLast1Hours / countLast1Hours

        patient["avgLast4Hours"] = format(avgLast4Hours, ".2f")
        patient["avgLast6Hours"] = format(avgLast6Hours, ".2f")
        patient["avgLast1andHalfHours"] = format(avgLast1andHalfHours, ".2f")
        patient["avgLast24Hours"] = format(avgLast24Hours, ".2f")
        patient["avgLast2Hours"] = format(avgLast2Hours, ".2f")
        patient["avgLast1Hours"] = format(avgLast1Hours, ".2f")

    print("Food Intaake Features extraction has been completed.")
    return corpus


def extractInsulinMedicationFeatures(corpus, attributes):
    # Insulin Medication Features
    #   * Average of Regular Insulin Level in the last 6 hours.
    #   * Average of NPH Insulin Level in the last 14 hours.
    #   * Average of Ultralente Insulin Level in the last 24 hours.
    #   * Maximum of Regular insulin in the last 6 hours.
    #   * Maximum of NPH insulin in the last 14 hours.
    #   * Maximum of Ultralente insulin in the last 24 hours.
    print("Extracting Insulin Medication Features...")
    regularInsulinCode = 0
    nphInsulinCode = 0
    ultralenteInsulinCode = 0

    for attr in attributes:
        if ("Regular" in attr["description"]):
            regularInsulinCode = attr["attrCode"]
        if ("NPH" in attr["description"]):
            nphInsulinCode = attr["attrCode"]
        if ("UltraLente" in attr["description"]):
            ultralenteInsulinCode = attr["attrCode"]

    for patient in corpus:
        totalRegularLast6Hours = 0
        countRegularLast6Hours = 0
        maxRegularLast6Hours = 0

        totalNPHLast14hours = 0
        countNPHLast14hours = 0
        maxNPHLast14Hours = 0

        totalUlralenteLast24Hours = 0
        countUltralenteLast24Hours = 0
        maxUltarelenteLast24Houts = 0

        patientTimestamp = float(patient["timestamp"])

        for p in corpus:
            pCode = int(p["attributeCode"])
            pValue = int(p["attributeValue"])
            pTimestamp = float(p["timestamp"])

            if pCode == regularInsulinCode:
                if pTimestamp <= patientTimestamp - (3600 * 6):
                    totalRegularLast6Hours += pValue
                    countRegularLast6Hours += 1
                    if pValue > maxRegularLast6Hours:
                        maxRegularLast6Hours = pValue
            if pCode == nphInsulinCode:
                if pTimestamp <= patientTimestamp - (3600 * 14):
                    totalNPHLast14hours += pValue
                    countNPHLast14hours += 1
                    if pValue > maxNPHLast14Hours:
                        maxNPHLast14Hours = pValue
            if pCode == ultralenteInsulinCode:
                if pTimestamp <= patientTimestamp - (3600 * 24):
                    totalUlralenteLast24Hours += pValue
                    countUltralenteLast24Hours += 1
                    if pValue > maxUltarelenteLast24Houts:
                        maxUltarelenteLast24Houts = pValue
        avgRegularLast6Hours = 0
        avgNPHLast14Hours = 0
        avgUltralenteLast24Hours = 0

        if countRegularLast6Hours > 0:
            avgRegularLast6Hours = totalRegularLast6Hours / countRegularLast6Hours
        if countNPHLast14hours > 0:
            avgNPHLast14Hours = totalNPHLast14hours / countNPHLast14hours
        if countUltralenteLast24Hours > 0:
            avgUltralenteLast24Hours = totalUlralenteLast24Hours / countUltralenteLast24Hours

        patient["avgRegularLast6Hours"] = format(avgRegularLast6Hours, ".2f")
        patient["avgNPHLast14Hours"] = format(avgNPHLast14Hours, ".2f")
        patient["avgUltralenteLast24Hours"] = format(
            avgUltralenteLast24Hours, ".2f")
        patient["maxRegularLast6Hours"] = format(maxRegularLast6Hours, ".2f")
        patient["maxNPHLast14Hours"] = format(maxNPHLast14Hours, ".2f")
        patient["maxUltarelenteLast24Houts"] = format(
            maxUltarelenteLast24Houts, ".2f")

    print("Insulin Medication Features extraction has been completed.")
    return corpus


def extractActivityFeatures(corpus, attributes):
    # Activity Features
    #   * Total Activity in the last 2 hours.
    #   * Total Activity in the last 4 hours.
    #   * Total Activity in the last 6 hours.
    #   * Total Activity in the last 12 hours.
    #   * Total Activity in the last 24 hours.
    #   * Average Activity in the last 2 hours.
    #   * Average Activity in the last 4 hours.
    #   * Average Activity in the last 6 hours.
    #   * Average Activity in the last 12 hours.
    #   * Average Activity in the last 24 hours.
    print("Extracting Activity Features...")
    activityCodes = []

    for attr in attributes:
        if attr["isExercise"] == 1:
            activityCodes.append(attr["attrCode"])

    for patient in corpus:
        totalLast2Hours = 0
        countLast2Hours = 0

        totalLast4Hours = 0
        countLast4Hours = 0

        totalLast6Hours = 0
        countLast6Hours = 0

        totalLast12Hours = 0
        countLast12Hours = 0

        totalLast24Hours = 0
        countLast24Hours = 0

        patientTimestamp = float(patient["timestamp"])

        for p in corpus:
            pCode = int(p["attributeCode"])
            pValue = int(p["attributeValue"])
            pTimestamp = float(p["timestamp"])

            for code in activityCodes:
                if code == pCode:
                    if pTimestamp <= patientTimestamp - (3600 * 2):
                        totalLast2Hours += pValue
                        countLast2Hours += 1
                    if pTimestamp <= patientTimestamp - (3600 * 4):
                        totalLast4Hours += pValue
                        countLast4Hours += 1
                    if pTimestamp <= patientTimestamp - (3600 * 6):
                        totalLast6Hours += pValue
                        countLast6Hours += 1
                    if pTimestamp <= patientTimestamp - (3600 * 12):
                        totalLast12Hours += pValue
                        countLast12Hours += 1
                    if pTimestamp <= patientTimestamp - (3600 * 24):
                        totalLast24Hours += pValue
                        countLast24Hours += 1
        avgLast2Hours = 0
        avgLast4Hours = 0
        avgLast6Hours = 0
        avgLast12Hours = 0
        avgLast24Hours = 0

        if countLast2Hours > 0:
            avgLast2Hours = totalLast2Hours / countLast2Hours
        if countLast4Hours > 0:
            avgLast4Hours = totalLast4Hours / countLast4Hours
        if countLast6Hours > 0:
            avgLast6Hours = totalLast6Hours / countLast6Hours
        if countLast12Hours > 0:
            avgLast12Hours = totalLast12Hours / countLast12Hours
        if countLast24Hours > 0:
            avgLast24Hours = totalLast24Hours / countLast24Hours

        patient["totalActivityLast2Hours"] = format(totalLast2Hours, ".2f")
        patient["totalActivityLast4Hours"] = format(totalLast4Hours, ".2f")
        patient["totalActivityLast6Hours"] = format(totalLast6Hours, ".2f")
        patient["totalActivityLast12Hours"] = format(totalLast12Hours, ".2f")
        patient["totalActivityLast24Hours"] = format(totalLast24Hours, ".2f")

        patient["avgActivityLast2Hours"] = format(avgLast2Hours, ".2f")
        patient["avgActivityLast4Hours"] = format(avgLast4Hours, ".2f")
        patient["avgActivityLast6Hours"] = format(avgLast6Hours, ".2f")
        patient["avgActivityLast12Hours"] = format(avgLast12Hours, ".2f")
        patient["avgActivityLast24Hours"] = format(avgLast24Hours, ".2f")

    print("Activity Feature extraction has been completed.")
    return corpus


def main():
    print("Feature Extraction script is running...")
    attributes, data = readFile(dataFilePath)
    corpus = cleanCorpus(data)
    # corpus = extractGeneralFeatures(corpus, attributes)
    corpus = extractBloodSugarFeatures(corpus, attributes)
    corpus = extractTimeRelatedFeatures(corpus, attributes)
    # corpus = extractFoodIntakeFeatures(corpus, attributes)
    # corpus = extractInsulinMedicationFeatures(corpus, attributes)
    # corpus = extractActivityFeatures(corpus, attributes)
    writeToCSV(corpus)
    writeToJSON(corpus, attributes)


main()
