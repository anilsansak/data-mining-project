import os
import datetime
import json
from datetime import timezone
from operator import itemgetter


global dataContainerPath, userCount, attributeCount, outputFolderPath, usersFileContent, datFileContent, lineDict, validLineCount

dataContainerPath = "./Diabetes-Data"
userCount = 0
attributeCount = 0
outputFolderPath = "./output"
usersFileContent = []
datFileContent = []
lineDict = {}
validLineCount = 0


def findDataFiles(folder):
    global userCount

    files = sorted(os.listdir(folder))
    for item in files:
        if("data-" in item):
            userCount += 1
            readFile(item, folder)


def readFile(fileName, folderName):
    global attributeCount, usersFileContent, userCount, datFileContent, lineDict, validLineCount
    filePath = folderName + "/" + fileName
    with open(filePath, "r") as f:
        fileLineCount = 0
        for line in f.readlines():
            fileLineCount += 1
            attributeCount += 1
            isValid = checkValid(line)
            if isValid is True:
                datFileContent.append({
                    "patientID": userCount,
                    "timestamp": lineDict["timestamp"],
                    "attributeCode": lineDict["code"],
                    "attributeValue": lineDict["value"]})
                validLineCount += 1
        usersFileContent.append(
            {"patientID": userCount, "attributeCount": fileLineCount})


def writeFile(fileName, folderName, content):
    # print("writing to", fileName)
    filePath = folderName + "/" + fileName
    with open(filePath, "w+") as f:
        f.write(content)


def checkValid(data):
    global lineDict
    splitted = data.split()
    try:
        timestamp = convertDateToTimestamp(splitted[0], splitted[1])
        date = splitted[0]
        time = splitted[1]
        code = splitted[2]
        value = splitted[3]
        if timestamp is None:
            return False
        else:
            lineDict = {"date": date, "time": time,
                        "code": code, "value": value, "timestamp": timestamp}
            return True
    except IndexError:
        print("error")
        return False


def convertDateToTimestamp(date, time):

    date_time_str = date + " " + time

    try:
        date_time_obj = datetime.datetime.strptime(
            date_time_str, '%m-%d-%Y %H:%M')
        timestamp = date_time_obj.replace(tzinfo=timezone.utc).timestamp()
        return timestamp
    except ValueError:
        print(date_time_str, "is not valid")
        return None


def createOutputFiles():
    global datFileContent

    datFileContent = sorted(datFileContent, key=itemgetter("timestamp"))
    singleOutputContent = {
        "info": {"userCount": userCount, "attributeCount": attributeCount}, "users": usersFileContent, "dat": datFileContent
    }

    writeFile("info.json", outputFolderPath, json.dumps(
        {"userCount": userCount, "attributeCount": attributeCount}))
    writeFile("users.json", outputFolderPath, json.dumps(usersFileContent))
    writeFile("dat.json", outputFolderPath, json.dumps(datFileContent))
    writeFile("singleOutput.json", outputFolderPath,
              json.dumps(singleOutputContent))


def main():
    global outputFolderPath, dataContainerPath, usersFileContent, userCount, attributeCount, validLineCount

    findDataFiles(dataContainerPath)
    createOutputFiles()
    print(validLineCount)


main()
