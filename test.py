import json


def readFile(filepath):
    print("Reading file:", filepath)
    with open(filepath) as json_file:
        data = json.load(json_file)
        return data


data = readFile("data.json")
print(data["attributes"][0])
